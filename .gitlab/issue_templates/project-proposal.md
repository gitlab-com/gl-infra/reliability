<!-- This template helps organize a project proposal so it can be considered to prioritization. -->
<!-- While the sections to not have to be exactly as structured here, a proposal should seek to provide answers to all the questions posed in the sections below. -->

<!-- If there are already issues related to this idea, these issues can be promoted to epics to relate the issues. Alternatively, issues can be related to the issue during the discussion and triaging phase of the project. -->

## Problem

<!-- What are we trying to solve? -->

<!-- Why is this important? -->

<!-- How does this fit within our team's area of ownership? -->

<!-- How does it fit into our roadmap and/or vision? -->

<!-- What happens if we choose not to do this? -->

## Goal

<!-- A concise summary of what we will do, how we will do it, and what impact it will have in 1-2 sentences. -->

## Exit Criteria

<!-- A list of milestones involved in this project. If all of these are complete, the project is complete. -->

## Proposal

<!-- What is your initial proposal of how we solve this problem? -->

<!-- What is the intented result/output/outcome of this work? -->

<!-- How will we know if it was successful? -->

<!-- What dependencies exist be it a project, another team, or something else? -->

<!-- Have you thought about how this affects other teams? -->

<!-- What level of effort is involved (how many SREs and for how long)? -->

### Alternatives

<!-- What alternatives have you considered? -->

<!-- How did you decide on your proposal over these alternatives? -->

/labels ~"workflow-infra::Triage" ~"group::Production Engineering" ~"Production Engineering::P4"
