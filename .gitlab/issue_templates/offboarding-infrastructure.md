Once created, please assign this issue to the Leaver's line manager for processing.

# Offboarding Infrastructure Engineer

* [ ] Find and destroy chef user
  * ssh into the chef server:
  * [ ] If the user was a server admin, remove from admins: `sudo cinc-server-ctl remove-server-admin-permissions CHEF_USERNAME`
  * [ ] If user was a chef admin, remove from admins group: `sudo cinc-server-ctl org-user-remove gitlab CHEF_USERNAME --force`
  * [ ] `sudo cinc-server-ctl user-delete CHEF_USERNAME`
* [ ] Remove user from `config/vault_admins.yml`. We no longer add engineers to this file by default, so they may not be listed.
  * [ ] run `bundle exec rake update_vault_admins`
* [ ] Remove user from gitlab ssh access:
  * [ ] `bundle exec knife data bag show users # find SSH-USERNAME`
  * [ ] `bundle exec rake "destroy_ssh_user[SSH-USERNAME]"` *NOTE*: please make sure that there's no critical nodes with chef-client explicitly disabled at the moment, such as DB, otherwise access will remain for this user.
* [ ] Remove user from the package server: `bundle exec rake 'edit_role[packages-gitlab-com]'`
* [ ] Remove user from their team in [infra-mgmt](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/tree/main/data/teams).
* [ ] Remove user from [Grafana](https://dashboards.gitlab.net).
* [ ] Remove user from [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/cloudflare/vendor.md#deprovisioning).

## Delivery specific

Additional step to complete if the team member was part of the [Delivery Group](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery)

* [ ] Remove user from Delivery Groups via [issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/new?issue[title]=Offboarding%20Delivery%20Team%20Member%20-%20%5Bname%5D&issuable_template=Offboarding-Delivery-team-member)

### Do Not Edit Below

/confidential
/label ~"workflow-infra::Triage" ~"Service::Infrastructure" ~"Production Engineering::P2" ~"team::Ops"
