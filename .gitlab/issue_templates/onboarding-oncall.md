<!--
  This issue supplements SRE onboarding for tasks that are specific to starting an oncall shift. It is recommended that you create this issue and ensure you are familiar with these items on a regular basis.
  
  This checklist is separate from the SRE onboarding issue because the systems we provision and tooling we support evolves over time. Where SRE onboarding covers everything you should *know*, this issue should over everything you should be able to *do* before going oncall.

  If you are already familiar with the item listed, you should feel free to check it without going through the exercise.
-->

# Title: Site Reliability Engineer Onboarding Issue 3 - Oncall Onboarding

[Fill in name and start date]

## Welcome to your oncall onboarding issue

This is the third and final of your onboarding issues. Below is a 12-week timeline to help you track your progress. While you can adjust the pace based on your experience and learning style, this structure helps ensure thorough preparation for oncall duties.

### Timeline Overview

- **Week 1**: Introduction and EOC Buddy assignment
- **Weeks 2-3**: Tools setup and incident investigation training
- **Weeks 4-5**: Incident response and management procedures
- **Weeks 6-7**: Security controls and delivery processes
- **Weeks 8-9**: Alert management and observability
- **Week 10-12**: Final assessment and transition to rotation

Progress through the checklist items below as you move through the timeline. You should aim to complete related items during their designated weeks, but can adjust based on your learning pace and buddy's guidance.

- [ ] When you are ready to work on this issue, set the label to ~"EOC-Onboarding::In-progress"

### EOC Buddy (Week 1)

You will have an Engineer On-call (EOC) onboarding buddy assigned to you to help you with your on-call onboarding experience. Review our [EOC Onboarding Buddies Handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/ops/sre-oncall-onboarding-buddy/) and [EOC Shadow and EOC Buddy Expectations](./on-call-shadowing.md).

At any time, feel free to ask your onboarding buddy for help, or reach out in `#eoc-shadowing`

- [ ] Join `#eoc-shadowing` Slack channel, introduce yourself and mention that you started your EOC Oncall Onboarding
- [ ] Message `@eoc-buddies` Slack group and mention that you are looking for an EOC-Buddy. Confirm your timezone and availability. You will have periodic sync calls with your future buddy and this information will help to find an EOC engineer in your timezone
- [ ] Wait for a new buddy to be assigned and schedule a first coffee chat with them. (You can continue with your onboarding while waiting for a buddy to be assigned)
- [ ] Discuss with your buddy if you want to have periodic sync calls to talk through the incidents and questions related to your Oncall Onboarding
- [ ] Join the [shadow rotation in PagerDuty](https://gitlab.pagerduty.com/schedules#PZEBYO0) ([managed via Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/pagerduty/sre_shadow_schedule.tf)) and align your shifts with the shifts of your EOC-buddy. This will help you to get the same alert notifications as your buddy and join sync Zoom calls for troubleshooting and review. You do not need to be a primary respondent (although you can if you want to) for incidents as this will help you to observe and grow your experience in the on-call process.
  - [ ] You might consider setting up a layer within the schedule to shadow on-call once a week on a consistent day. This will allow you to work with different people and see different issues over time, without as much mental drain as a full week on-call

### Incident Investigation and Core Tools (Weeks 2-3)

An incident starts with trying to efficiently identify the nature of the problem, drilling down from:

- PagerDuty alert
- Grafana dashboard for the alerting service
- Kibana log events for that service, often starting with one of the quick links from the Grafana dashboard
- Possibly looking at other Grafana dashboards if the above indicates that the alerting service is having trouble due to its dependency on another service (e.g. Rails having lots of SQL statement timeouts may indicate trouble on the database or its connection pooler).

Once we identify the affected component and the nature of its problem, that usually gives us enough info to understand what kind of solutions are likely to be helpful -- and that may mean getting help from domain experts in whatever component of the app code or infrastructure that we identified as contributing causes of the incident.

Remember that you are not alone. At any point you can ask for help from other SREs in the #infrastructure-lounge channel, someone will be happy to join you in zoom. You can also escalate to the Incident Manager On Call (IMOC) at any time, if you need a second opinion, a different perspective, need help knowing who to reach out to on other teams, etc.

If you encounter any suspicious activity during investigations, immediately [contact security](https://handbook.gitlab.com/handbook/security/security-operations/sirt/engaging-security-on-call/).

During these weeks, you'll learn the essential investigative process and tools for handling incidents effectively.

#### Investigation Methodology

An incident investigation follows a structured approach, drilling down from:

- [ ] Learn PagerDuty alert analysis:
  - Study alert patterns and severity levels
  - Understand service mapping to alerts
  - Practice interpreting alert context
  - Review [alert documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/alerts_manual.md)
- [ ] Master Grafana dashboard investigation:
  - Navigate service-specific dashboards
  - Interpret key metrics and trends
  - Understand performance indicators
  - Correlate metrics across services
  - Practice using the [general: SLAs dashboard](https://dashboards.gitlab.net/d/general-slas/general-slas)
- [ ] Practice Kibana log analysis:
  - Use quick links from Grafana effectively
  - Filter and search log events
  - Identify error patterns
  - Analyze log frequencies
  - Follow the [Kibana documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/elastic/kibana.md)
- [ ] Study service dependencies:
  - Understand common failure patterns
  - Recognize database connection issues
  - Identify service dependencies
  - Track request flows through systems
  - Review [runbooks](https://gitlab.com/gitlab-com/runbooks)

#### Essential Tools and Hands-on Practice

- [ ] Set up and configure [Woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse/):
  - Complete initial installation
  - Test core functionality
  - Practice common operations
  - Follow setup documentation
- [ ] Master the sre-oncall Slack bot:
  - Learn `/sre-oncall handover` procedures
  - Study [handover documentation](https://about.gitlab.com/handbook/engineering/infrastructure/team/ops/on-call-handover/)
  - Practice handover communications
- [ ] Configure monitoring tools:
  - Set up personal Grafana dashboards
  - Create Kibana visualizations using [documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/elastic/kibana.md)
  - Test alert notifications
  - Practice using quick links
  - Ensure you can run `make generate` in [runbooks](https://gitlab.com/gitlab-com/runbooks)
- [ ] Set up Kubernetes access:
  - Follow [k8s setup documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md)
  - Verify access to zonal and regional clusters
  - Test basic operations

#### Key Investigation Scenarios

Practice investigating these common scenarios:

- [ ] Rails performance issues
  - Review [Apdex alerts troubleshooting](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/apdex-alerts-guide.md)
- [ ] Database connection problems
  - Study [HAProxy documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/haproxy.md)
- [ ] CI/CD pipeline failures
  - Review [CI runner documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/ci-apdex-violating-slo.md)
- [ ] Storage system alerts
  - Study relevant runbooks
- [ ] Network connectivity issues
  - Review [HAProxy state management](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/haproxy.md#set-server-state)
  
### Incident Management (Weeks 4-5)

#### Asking for help

Make sure you know how to:

- [ ] Page IMOC by typing `/pd trigger` in slack, then choosing `GitLab Production - Incident Manager` under `Impacted Service`
- [ ] Page CMOC by typing `/pd trigger` in slack, then choosing `Incident Management - CMOC` under `Impacted Service`
- [ ] Page Security - for medium/high severity incidents, refer to [how to engage the SEOC](https://handbook.gitlab.com/handbook/security/security-operations/sirt/engaging-security-on-call/#engage-the-security-engineer-on-call). For lower severity incidents, refer to the [incident severity table](https://handbook.gitlab.com/handbook/security/security-operations/sirt/engaging-security-on-call/#incident-severity) to determine the right course of action
- [ ] Page Dev by typing `/devoncall incident-issue-url` into `#dev-escalation`. [Handbook](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#process-outline)

#### Core Procedures

- [ ] To declare an incident via Slack: `/incident declare`
- [ ] Checkout an example alert in `#production`, explore the `Runbook`, `Dashboard`, the description and the related `Prometheus graph` by clicking `show more`. Note that any of these links could be outdated, so proceed your evaluation with caution
- [ ] Understand when an Incident Review is required by viewing [The Incident Review Handbook](https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review)
- [ ] Checkout Scenario 3 YouTube recording in [this Firedrill doc](https://docs.google.com/document/d/1uZHz1w3NC6yhSPpuWiUftoz2pIaMtnXhKGvn4O3Fe1U/edit#heading=h.o4psext022tb) to give you an idea of the k8s-related issues you might encounter in gitlab.com

### Security and Delivery (Weeks 6-7)

#### Security Controls

- [ ] Explain all traffic policing mechanisms we have available. [View the runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/rate-limiting/README.md)
- [ ] How to block a user? [Runbook for dealing with CI/CD Abuse](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/ci-apdex-violating-slo.md#abuse)
- [ ] How to add a rate limit for a path? [View the runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/rate-limiting/README.md#how-tos)
- [ ] Disabling things in HAProxy. [Project import - Block example](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/block-things-in-haproxy.md#block-project-imports-using-blacklist)

#### Delivery Processes

- [ ] Create a hot-patch against production with a single change to a source file that adds a comment. Assign the MR to one of the [current release managers](https://about.gitlab.com/community/release-managers/). [view documentation](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/post-deployment-patches.md)
- [ ] Get the current state of GitLab.com Canary stage using GitLab Chatops. [view documentation](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/canary.md)
- [ ] Find the latest auto-deploy pipeline on ops.gitlab.net and get the current deploy status on all environments using GitLab Chatops. [view documentation](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/auto-deploy.md)
- [ ] Setup your workstation to ensure you have access to the zonal and regional k8s clusters. [view documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md)

### Observability and Reliability (Weeks 8-9)

#### Dashboards and Monitoring

- [ ] Take a look at the following dashboards:
  - [ ] Locate the `general: SLAs` dashboard
  - [ ] Locate the `sidekiq: Overview` and find panel for `Sidekiq Queue Lengths per Queue`
- [ ] Read these documents about notifications and troubleshooting:
  - [ ] [Tuning and Modifying Alerts](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/alert_tuning.md)
  - [ ] [Apdex alerts troubleshooting](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/apdex-alerts-guide.md)
  - [ ] [An impatient SRE's guide to deleting alerts](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/deleting-alerts.md)
- [ ] Ensure you know how to Silence an alert. [view documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/alerts_manual.md)
- [ ] Ensure you can run `make generate` in [runbooks](https://gitlab.com/gitlab-com/runbooks)
- [ ] Create a visualization in Kibana for all errors grouped by status code. [view documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/elastic/kibana.md)

#### Reliability Tasks

- [ ] Familiarize yourself with how to create incidents from Slack. [view documentation](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#report-an-incident-via-slack)
- [ ] Get the current HAProxy state of all nodes using the command line. [view documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/haproxy.md)
- [ ] First drain and then ready connections from one of the zonal clusters in staging. [view documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/haproxy.md#set-server-state)
- [ ] Join the following slack channels: `#incident-management`, `#production`, `#releases`, `#f_upcoming_release`, `#feed_alerts-general`, `#alerts`, `#dev-escalation`

### Final Assessment and Transition (Week 10-12)

#### Alerts And Metrics Review

- [ ] Review [Alerting on SLOs](https://sre.google/workbook/alerting-on-slos/#ch05fn5)
- [ ] [Review Alert documentation](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/alerts_manual.md)
- [ ] [How to silence Alerts](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/alerts_manual.md#silencing)
- [ ] [Review active alert silences](https://alerts.gitlab.net/#/silences)
- [ ] [Review the Metrics Catalog](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/README.md)

#### Infrastructure Gamedays

To meet our RTO and RTO objectives we have a [Disaster Recovery Working Group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/). Periodic scheduled [Infrastructure Gamedays](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=created_date&state=closed&label_name%5B%5D=gamedays&first_page_size=100) are executed on our staging and production environments to improve our Disaster Recovery capabilities.

- [ ] Review [Disaster Recovery Working Group Handbook page](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/)
- [ ] [OPTIONAL] If you would like to participate and run a Gameday yourself, contact Ops Team [#g_infra_ops](https://gitlab.enterprise.slack.com/archives/C04MH2L07JS) and join [#f_gamedays](https://gitlab.enterprise.slack.com/archives/C07PV3F6J1W) channel
- [ ] [OPTIONAL] Schedule a gameday for a component and run it. Record in the comments of this issue

#### On Call Shadowing

- [ ] Join the [shadow rotation in PagerDuty](https://gitlab.pagerduty.com/schedules#PZEBYO0) ([managed via Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/pagerduty/sre_shadow_schedule.tf)) for a complete week and shadow a current oncall.
  - [ ] As several SRE follow different processes, it's recommended to reach out to the SRE oncall and if possible ask them to join the zoom link to discuss the alerts that fire to observe the troubleshooting actions
- [ ] After joining the shadow rotation, update the label on this issue to ~"EOC-Onboarding::Shadowing"
- [ ] Join the [shadow rotation in PagerDuty](https://gitlab.pagerduty.com/schedules#PZEBYO0) ([managed via Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/pagerduty/sre_shadow_schedule.tf)) a second time and communicate with the EOC that you will take primary with them as a fallback. Record a log of by linking the issue links:
  - How many alerts you acknowledge
    - [ ] #
    - [ ] #
    - [ ] #
    - [ ] #
  - How many alerts felt actionable
    - [ ] #
    - [ ] #
  - How many alerts "made sense", where you knew what you needed to do or where to look
    - [ ] #
    - [ ] #
  - Ideally, you are ready to join when the ratio of made sense to acknowledge is above 80% with working on at least 10 alerts

### Add to SRE rotation

- [ ] Ask for a review from your onboarding buddy and if ready open an [MR](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/pagerduty/sre_locals.tf) to update the regional PagerDuty rotation
- [ ] Provide your feedback in the [EOC-Onboarding Feedback Form](https://docs.google.com/forms/d/e/1FAIpQLScigjnR6uBv7WNYboassqV2infCDaQEqLSTP_eYIdNYlEShng/viewform)
- [ ] Update the label on this issue to ~"EOC-Onboarding::Ready"

/label ~"workflow-infra::Ready" ~"group::Production Engineering" ~"onboarding" ~"EOC" ~"EOC-Onboarding::New"

/due in 3 months

/confidential
